#ifndef OSEFI2CDEVICE_H
#define OSEFI2CDEVICE_H

#include "SMBusDevice.h"

namespace OSEF
{
    const uint16_t I2C_WRITE = 0;
    const uint16_t I2C_READ = 1;

    const uint16_t I2C_RDWR = 0x0707;

    typedef struct
    {
        uint16_t address;
        uint16_t flags;
        uint16_t length;
        uint8_t *buffer;
    }i2c_ioctl_msg;

    typedef struct
    {
        i2c_ioctl_msg* msg;
        uint32_t n;
    }i2c_ioctl_arg;

    class I2CDevice : public SMBusDevice
    {
    public:
        I2CDevice(const uint8_t& adp, const uint8_t& dvc);
        virtual ~I2CDevice();

        bool i2cWriteByte(const uint8_t& value);

        bool i2cReadWord(uint16_t& value);
        bool i2cReadWordSwap(uint16_t& value);

        bool i2cWBReadWord(const uint8_t& wu8, uint16_t& ru16, const uint16_t& to);
        bool i2cWBReadWordSwap(const uint8_t& wu8, uint16_t& ru16, const uint16_t& to);

        bool i2cReadBuffer(uint8_t* buffer, const uint8_t& size);

        bool i2cWBReadBuffer(const uint8_t& wu8, uint8_t* buffer, const uint8_t& size, const uint16_t& to);

    private:
        I2CDevice(const I2CDevice& orig);
        I2CDevice& operator=(const I2CDevice& orig);

        bool i2cExchange(i2c_ioctl_msg* msgs, const uint32_t& nmsgs);

        bool _writeByte(const uint8_t& value);  // unprotected

        bool _readWord(uint16_t& value);  // unprotected

        bool _readBuffer(uint8_t* buffer, const uint8_t& size);  // unprotected
    };
}  // namespace OSEF

#endif /* OSEFI2CDEVICE_H */
