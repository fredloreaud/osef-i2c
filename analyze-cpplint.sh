#!/bin/bash

# build/c++11 is discarded to implement C++11 std::thread
CPPLINT_FILTER=-build/c++11

# build/header_guard should check uniqueness and not a cumbersome pattern
CPPLINT_FILTER+=,-build/header_guard

# runtime/references is discarded as passing a pointer is more prone to errors ...
CPPLINT_FILTER+=,-runtime/references

# runtime/string forbids static strings then decrease readability and runtime performances when string needs to be constructed from char*
CPPLINT_FILTER+=,-runtime/string

# runtime/arrays merely checks array size begin with letter k notit really is a constant ...
CPPLINT_FILTER+=,-runtime/arrays

# runtime/indentation_namespace forbids indentation in namespace which does not imprve readability
CPPLINT_FILTER+=,-runtime/indentation_namespace

# legal/copyright is redundant with repo LICENSE file
CPPLINT_FILTER+=,-legal/copyright

# whitespace/newline doesn't allow else statement as same level as corresponding if ...
CPPLINT_FILTER+=,-whitespace/newline

# whitespace/line_length 80 characters limit is obsolete for some time already ...
CPPLINT_FILTER+=,-whitespace/line_length

# whitespace/braces requires opening brace at end of line which does not improve readability
CPPLINT_FILTER+=,-whitespace/braces

# whitespace/indent doesn't support indent after #ifdef
CPPLINT_FILTER+=,-whitespace/indent

# readability/alt_tokens requires to replace operator not by ! which does not improve readability
CPPLINT_FILTER+=,-readability/alt_tokens

cpplint --version

# run analysis only if source are provided
if [ -z "$1" ]
then
    echo Please provide sources i.e. "*/*.h */*.cpp"
else
    cpplint --filter=$CPPLINT_FILTER "$@"
fi
