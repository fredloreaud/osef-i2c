#include "I2CDevice.h"
#include "Swap.h"
#include "TimeOut.h"
#include "Debug.h"

#include <sys/ioctl.h>  // ioctl

OSEF::I2CDevice::I2CDevice(const uint8_t& adp, const uint8_t& dvc)
    :SMBusDevice(adp, dvc) {}

OSEF::I2CDevice::~I2CDevice() {}

bool OSEF::I2CDevice::i2cExchange(i2c_ioctl_msg* msgs, const uint32_t& nmsgs)
{
    bool ret = false;

    i2c_ioctl_arg arg;
    arg.msg = msgs;
    arg.n = nmsgs;

    int rctl = ioctl(file, I2C_RDWR, &arg);
    if (rctl == 1)  // to be checked when exchanging more than one message at a time
    {
        ret = true;
    }
    else
    {
        SF_SMER("error i2c read " << strerror(errno));
    }

    return ret;
}

bool OSEF::I2CDevice::i2cReadBuffer(uint8_t* buffer, const uint8_t& size)
{
    bool ret = false;

    ret = _readBuffer(buffer, size);

    return ret;
}

bool OSEF::I2CDevice::i2cWBReadBuffer(const uint8_t& wu8, uint8_t* buffer, const uint8_t& size, const uint16_t& to)
{
    bool ret = false;

    if (_writeByte(wu8))
    {
        if (OSEF::sleepms(to))
        {
            ret = _readBuffer(buffer, size);
        }
    }

    return ret;
}

bool OSEF::I2CDevice::_readBuffer(uint8_t* buffer, const uint8_t& size)
{
    i2c_ioctl_msg msgs[1];

    msgs[0].address = device;
    msgs[0].flags = I2C_READ;
    msgs[0].buffer = buffer;
    msgs[0].length = size;

    const bool ret = i2cExchange(&msgs[0], 1);

    return ret;
}

bool OSEF::I2CDevice::i2cWriteByte(const uint8_t& value)
{
    bool ret = false;

    ret = _writeByte(value);

    return ret;
}

bool OSEF::I2CDevice::_writeByte(const uint8_t& value)
{
    i2c_ioctl_msg msgs[1];

    uint8_t tmpValue = value;

    msgs[0].address = device;
    msgs[0].flags = I2C_WRITE;
    msgs[0].buffer = &tmpValue;
    msgs[0].length = 1;

    const bool ret = i2cExchange(&msgs[0], 1);

    return ret;
}

bool OSEF::I2CDevice::i2cWBReadWord(const uint8_t& wu8, uint16_t& ru16, const uint16_t& to)
{
    bool ret = false;

    if (_writeByte(wu8))
    {
        if (OSEF::sleepms(to))
        {
            ret = _readWord(ru16);
        }
    }

    return ret;
}

bool OSEF::I2CDevice::i2cReadWord(uint16_t& value)
{
    bool ret = false;

    ret = _readWord(value);

    return ret;
}

bool OSEF::I2CDevice::_readWord(uint16_t& value)
{
    i2c_ioctl_msg msgs[1];

    msgs[0].address = device;
    msgs[0].flags = I2C_READ;
    msgs[0].buffer = reinterpret_cast<uint8_t*>(&value);
    msgs[0].length = 2;

    const bool ret = i2cExchange(&msgs[0], 1);

    return ret;
}

bool OSEF::I2CDevice::i2cReadWordSwap(uint16_t& value)
{
    bool ret = i2cReadWord(value);
    value = swap16(value);
    return ret;
}

bool OSEF::I2CDevice::i2cWBReadWordSwap(const uint8_t& wu8, uint16_t& ru16, const uint16_t& to)
{
    bool ret = i2cWBReadWord(wu8, ru16, to);
    ru16 = swap16(ru16);
    return ret;
}
