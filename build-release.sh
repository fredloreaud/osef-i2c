#!/bin/bash

# log display macros
normal=$(tput sgr0)
green=$(tput setaf 2)

echostep ()
{
    echo
    echo ${green}"____"$@"____"${normal}
    echo
}

# setup directory to execute build and store artifacts
echostep "set build directory"

mkdir -p build/release

cd build/release

# generate build files with cmake
echostep "generate build files"

cmake -DCMAKE_BUILD_TYPE=release -DCMAKE_EXPORT_COMPILE_COMMANDS=ON ../..

# build target with make
echostep "build target"

make

echostep "release build script end"
