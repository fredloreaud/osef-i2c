#!/bin/bash

# count lines only if source are provided
if [ -z "$1" ]
then
    echo Please provide sources i.e. "*/*.h */*.cpp"
    echo You may also --exclude-dir exclude/this/directory
else
    cloc "$@"
fi
