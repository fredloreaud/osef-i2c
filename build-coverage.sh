#!/bin/bash

mkdir -p build/coverage

cd build/coverage

cmake -DCMAKE_BUILD_TYPE=coverage ../..

make
