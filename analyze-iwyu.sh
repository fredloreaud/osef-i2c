#!/bin/bash

mkdir -p build/release

cd build/release

cmake -DCMAKE_BUILD_TYPE=release "-DCMAKE_CXX_INCLUDE_WHAT_YOU_USE=iwyu;--transitive_includes_only" ../..

make
