#!/bin/bash

# https://clang.llvm.org/extra/clang-tidy/checks/list.html

CLANG_TIDY_CHECKS="*"

CLANG_TIDY_CHECKS+=",abseil*"

CLANG_TIDY_CHECKS+=",altera*"
CLANG_TIDY_CHECKS+=",-altera-struct-pack-align" # alignement optimization

CLANG_TIDY_CHECKS+=",android*"

CLANG_TIDY_CHECKS+=",boost*"

CLANG_TIDY_CHECKS+=",bugprone*"

CLANG_TIDY_CHECKS+=",cert*"

CLANG_TIDY_CHECKS+=",clang*"

CLANG_TIDY_CHECKS+=",concurrency*"

CLANG_TIDY_CHECKS+=",cppcoreguidelines*"
CLANG_TIDY_CHECKS+=",-cppcoreguidelines-non-private-member-variables-in-classes" # aliases misc-non-private-member-variables-in-classes
CLANG_TIDY_CHECKS+=",-cppcoreguidelines-avoid-magic-numbers" # constexpr ?
CLANG_TIDY_CHECKS+=",-cppcoreguidelines-pro-type-union-access" # forbids sa_sigaction usage
CLANG_TIDY_CHECKS+=",-cppcoreguidelines-avoid-c-arrays" # aliases modernize-avoid-c-arrays
CLANG_TIDY_CHECKS+=",-cppcoreguidelines-pro-type-vararg" # forbids mq_open usage
CLANG_TIDY_CHECKS+=",-cppcoreguidelines-pro-type-reinterpret-cast"
CLANG_TIDY_CHECKS+=",-cppcoreguidelines-pro-type-cstyle-cast"
CLANG_TIDY_CHECKS+=",-cppcoreguidelines-owning-memory" # affects ThreadSpawner
CLANG_TIDY_CHECKS+=",-cppcoreguidelines-pro-bounds-constant-array-index" # forbids FD_SET usage
CLANG_TIDY_CHECKS+=",-cppcoreguidelines-avoid-non-const-global-variables" # forbids private static members

CLANG_TIDY_CHECKS+=",darwin*"

CLANG_TIDY_CHECKS+=",-fuchsia*" # necessary for clang-tidy older version on GitLab ...
CLANG_TIDY_CHECKS+=",-fuchsia-overloaded-operator"
CLANG_TIDY_CHECKS+=",-fuchsia-default-arguments-declarations"
CLANG_TIDY_CHECKS+=",-fuchsia-default-arguments-calls"
CLANG_TIDY_CHECKS+=",-fuchsia-statically-constructed-objects"
CLANG_TIDY_CHACKERS+=",-fuchsia-default-arguments*" # necessary for clang-tidy older version on GitLab ...

CLANG_TIDY_CHECKS+=",google*"
CLANG_TIDY_CHECKS+=",-google-runtime-references"
CLANG_TIDY_CHECKS+=",-google-readability-casting"

# High Integrity C++ (https://www.wikiwand.com/en/High_Integrity_C%2B%2B)
CLANG_TIDY_CHECKS+=",hicpp*"
CLANG_TIDY_CHECKS+=",-hicpp-vararg" # aliases cppcoreguidelines-pro-type-vararg
CLANG_TIDY_CHECKS+=",-hicpp-signed-bitwise" # forbids FD_SET macro usage
CLANG_TIDY_CHECKS+=",-hicpp-no-assembler" # forbids FD_SET macro usage
CLANG_TIDY_CHECKS+=",-hicpp-avoid-c-arrays" # aliases modernize-avoid-c-arrays

CLANG_TIDY_CHECKS+=",linuxkernel*"

CLANG_TIDY_CHECKS+=",llvm*"
CLANG_TIDY_CHECKS+=",-llvm-header-guard" # enforces header guard out of absolute path
CLANG_TIDY_CHECKS+=",-llvm-include-order" # enforces lexicographic include order
CLANG_TIDY_CHECKS+=",-llvmlibc-restrict-system-libc-headers" # forbids system libc vs llvm-libc
CLANG_TIDY_CHECKS+=",-llvmlibc-implementation-in-namespace" # allows only __llvm_libc namespace
CLANG_TIDY_CHECKS+=",-llvmlibc-callee-namespace" # forbids std namespace vs __llvm_libc

CLANG_TIDY_CHECKS+=",misc*"

CLANG_TIDY_CHECKS+=",modernize*"
CLANG_TIDY_CHECKS+=",-modernize-use-trailing-return-type" # purely stylistic transformation increasing code size
CLANG_TIDY_CHECKS+=",-modernize-use-using" # prompts gcc subobject-linkage warning
CLANG_TIDY_CHECKS+=",-modernize-avoid-c-arrays"
CLANG_TIDY_CHECKS+=",-modernize-pass-by-value" # alters passed parameter

CLANG_TIDY_CHECKS+=",mpi*"

CLANG_TIDY_CHECKS+=",objc*"

CLANG_TIDY_CHECKS+=",openmp*"

CLANG_TIDY_CHECKS+=",performance*"

CLANG_TIDY_CHECKS+=",portability*"

CLANG_TIDY_CHECKS+=",readability*"
CLANG_TIDY_CHECKS+=",-readability-convert-member-functions-to-static"
CLANG_TIDY_CHECKS+=",-readability-magic-numbers"
CLANG_TIDY_CHECKS+=",-readability-isolate-declaration" # forbibs FD_ZERO usage

CLANG_TIDY_CHECKS+=",zircon*"


clang-tidy --version

mkdir -p build/release

cd build/release

cmake -DCMAKE_BUILD_TYPE=release "-DCMAKE_CXX_CLANG_TIDY=clang-tidy;-warnings-as-errors=*;-header-filter=.;-line-filter=[{'name':'gtest.cc','lines':[[1,1]]}];-checks=$CLANG_TIDY_CHECKS" ../..
#cmake -DCMAKE_BUILD_TYPE=release "-DCMAKE_CXX_CLANG_TIDY=clang-tidy;-fix;-header-filter=.;-checks=$CLANG_TIDY_CHECKS" ../..
#cmake -DCMAKE_BUILD_TYPE=release -DCLANG_TIDY=ON ../..

make clean

make
