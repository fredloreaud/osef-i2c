#!/bin/bash

cd build/coverage

./osef-gtest "$@"

gcovr . --exclude-directories CMakeFiles --exclude-directories googletest-build -r ../../.. --html --html-details -o index.html
